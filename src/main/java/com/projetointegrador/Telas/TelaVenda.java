/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.projetointegrador.Telas;

import com.projetointegrador.DAO.ClienteDAO;
import com.projetointegrador.DAO.ProdutoDAO;
import com.projetointegrador.DAO.VendaDao;
import com.projetointegrador.model.Clientes;
import com.projetointegrador.model.Produto;
import com.projetointegrador.model.Venda;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import validadores.Validador;

/**
 *
 * @author Rafael
 *
 */
public class TelaVenda extends javax.swing.JFrame {

     int valorTotal = 0;
     int qtdeTotal = 0;
    /**
     * Creates new form TelaVenda
     */
    public TelaVenda() {
        initComponents();
        Calendar date = Calendar.getInstance();
        jdcDataVenda.setDate(date.getTime());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jCalendar1 = new com.toedter.calendar.JCalendar();
        jMonthChooser1 = new com.toedter.calendar.JMonthChooser();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnExcluir = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbVenda = new javax.swing.JTable();
        btnFinalizar = new javax.swing.JButton();
        txtTotal1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtCarrinho = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        txtProduto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jdcDataVenda = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        txtDescricaoProduto = new javax.swing.JTextField();
        txtQtde = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        txtBuscarCliente = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        txtNomeCliente = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtEstoque = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtVendedor = new javax.swing.JComboBox<>();
        btnInterir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(750, 550));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        btnExcluir.setBackground(new java.awt.Color(89, 5, 123));
        btnExcluir.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnExcluir.setForeground(new java.awt.Color(255, 255, 255));
        btnExcluir.setText("EXCLUIR ITEM");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        tbVenda.setBackground(new java.awt.Color(255, 251, 245));
        tbVenda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cód. Produto", "Tipo Prod.", "Qntd Estoq.", "Valor Prod."
            }
        ));
        jScrollPane2.setViewportView(tbVenda);

        btnFinalizar.setBackground(new java.awt.Color(89, 5, 123));
        btnFinalizar.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnFinalizar.setForeground(new java.awt.Color(255, 255, 255));
        btnFinalizar.setText("FINALIZAR VENDA");
        btnFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarActionPerformed(evt);
            }
        });

        txtTotal1.setEditable(false);
        txtTotal1.setBackground(new java.awt.Color(255, 251, 245));

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("TOTAL ADICIONADO");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("ITEMS NO CARRINHO");

        txtCarrinho.setEditable(false);
        txtCarrinho.setBackground(new java.awt.Color(255, 251, 245));
        txtCarrinho.setForeground(new java.awt.Color(0, 0, 0));

        jPanel5.setBackground(new java.awt.Color(89, 5, 123));

        txtProduto.setBackground(new java.awt.Color(255, 255, 255));
        txtProduto.setForeground(new java.awt.Color(0, 0, 0));
        txtProduto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtProdutoFocusLost(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel6.setText("Produto:");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel3.setText("Data Venda:");

        jdcDataVenda.setBackground(java.awt.Color.white);

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel7.setText("Nome Produto:");

        txtDescricaoProduto.setEditable(false);
        txtDescricaoProduto.setBackground(new java.awt.Color(255, 255, 255));
        txtDescricaoProduto.setForeground(new java.awt.Color(0, 0, 0));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel8.setText("Quantidade:");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel1.setText("CPF Cliente:");

        txtValor.setEditable(false);
        txtValor.setBackground(new java.awt.Color(255, 255, 255));
        txtValor.setForeground(new java.awt.Color(0, 0, 0));

        txtBuscarCliente.setBackground(java.awt.Color.white);
        txtBuscarCliente.setForeground(new java.awt.Color(0, 0, 0));
        try {
            txtBuscarCliente.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarClienteActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel10.setText("Valor unid:");

        txtNomeCliente.setEditable(false);
        txtNomeCliente.setBackground(java.awt.Color.white);
        txtNomeCliente.setForeground(new java.awt.Color(0, 0, 0));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel5.setText("Nome Cliente:");

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel12.setText("Estoque");

        txtEstoque.setEditable(false);
        txtEstoque.setBackground(new java.awt.Color(255, 255, 255));
        txtEstoque.setForeground(new java.awt.Color(0, 0, 0));

        jLabel13.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel13.setText("Vendedor");

        txtVendedor.setBackground(new java.awt.Color(255, 255, 255));
        txtVendedor.setForeground(new java.awt.Color(0, 0, 0));
        txtVendedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione...", "Rafael Ribeiro", "Michael Evangelista", "Malcon Miguel", "Eduardo Fernandes", "Gabriel Gasparoto", " " }));

        btnInterir.setBackground(new java.awt.Color(255, 255, 255));
        btnInterir.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnInterir.setForeground(new java.awt.Color(89, 5, 123));
        btnInterir.setText("INSERIR");
        btnInterir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInterirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jdcDataVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(txtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(txtBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(txtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(txtEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtQtde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtDescricaoProduto, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                                            .addComponent(jLabel12)
                                            .addGap(18, 18, 18)
                                            .addComponent(jLabel8))))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnInterir, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jdcDataVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13)
                        .addGap(3, 3, 3)
                        .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescricaoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jLabel8)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQtde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtValor)
                            .addComponent(btnInterir)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(txtCarrinho, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap())
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(40, 40, 40))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(42, 42, 42))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(txtTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap())))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                                    .addComponent(btnExcluir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCarrinho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 641, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnInterirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInterirActionPerformed
        Validador validador = new Validador();

        validador.ValidarVazio(txtProduto);
        validador.ValidarVazio(txtDescricaoProduto);
        validador.ValidarVazioJS(txtQtde);
        validador.ValidarVazio(txtValor);
        validador.ValidarVazio(txtEstoque);
        validador.mensagem();

        int cod_produto = Integer.parseInt(txtProduto.getText());
        String descricao = txtDescricaoProduto.getText();
        int qtd = Integer.parseInt(txtQtde.getValue().toString());
        double valor = Double.parseDouble(txtValor.getText());

        int quantidade = Integer.parseInt(txtQtde.getValue().toString());
        int estoque = Integer.parseInt(txtEstoque.getText());

        if (quantidade <= estoque && quantidade != 0) {

            DefaultTableModel modelo = (DefaultTableModel) tbVenda.getModel();
            modelo.addRow(new String[]{
                String.valueOf(cod_produto),
                descricao,
                String.valueOf(qtd),
                String.valueOf(valor)
            }
        );

        double valorLinha = qtd * valor;
        valorTotal += valorLinha;

        int valorLinhaQtd = qtd;
        qtdeTotal += valorLinhaQtd;

        txtTotal1.setText(String.valueOf(valorTotal));
        txtCarrinho.setText(Integer.toString(qtdeTotal));

        limparTexto();

        } else {
            JOptionPane.showMessageDialog(null, "Erro na compra, escolha uma quantidade menor ao disponível em estoque e diferente de Zero");
        }
    }//GEN-LAST:event_btnInterirActionPerformed

    private void txtBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBuscarClienteActionPerformed

        if(!txtBuscarCliente.getText().trim().equals("")){
            String CPF = txtBuscarCliente.getText();
            Clientes prod = ClienteDAO.consultar(CPF);

            if (prod != null) {
                txtNomeCliente.setText(prod.getNome());
            } else {
                JOptionPane.showMessageDialog(null, "Cliente não encontrado!");
                limparTextoGeral();
            }
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscarClienteActionPerformed

    private void txtProdutoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtProdutoFocusLost
        if(!txtProduto.getText().trim().equals("")){
            int id = Integer.parseInt(txtProduto.getText());
            Produto prod = ProdutoDAO.consultarPorID(id);

            if (prod != null) {
                txtDescricaoProduto.setText(prod.getNomeProduto());
                txtValor.setText(String.valueOf(prod.getValorProduto()));
                txtEstoque.setText(String.valueOf(prod.getQuantidadeProduto()));

            } else {
                JOptionPane.showMessageDialog(null, "Produto não encontrado!");
            }
        }
    }//GEN-LAST:event_txtProdutoFocusLost
    
    private void btnFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarActionPerformed
        Validador validador = new Validador();
        validador.ValidarVazioJDC(jdcDataVenda);
        validador.ValidarVazioJ(txtBuscarCliente);
        validador.ValidarVazio(txtNomeCliente);
        validador.ValidarVazioJCB(txtVendedor);
        validador.mensagem();

        ArrayList<Produto> listaItens = new ArrayList<Produto>();
        if(tbVenda.getRowCount() >=0){
            for(int i=0;i<tbVenda.getRowCount();i++){
                Produto item = new Produto();

                item.setCodProduto(tbVenda.getValueAt(i, 0).toString());
                item.setNomeProduto(tbVenda.getValueAt(i, 1).toString());
                item.setQuantidadeProduto(Integer.parseInt(tbVenda.getValueAt(i, 2).toString()));
                item.setValorProduto(Double.parseDouble(tbVenda.getValueAt(i,3 ).toString()));

                listaItens.add(item);
            }
        }

        txtTotal1.setText(String.valueOf(valorTotal));
        txtCarrinho.setText(Integer.toString(qtdeTotal));

        Double valor = Double.parseDouble(txtTotal1.getText());
        Date DataVenda = jdcDataVenda.getDate();
        String cpfVenda = txtBuscarCliente.getText();
        String nomeCliente = txtNomeCliente.getText();
        String nomeProduto = txtDescricaoProduto.getText();

        Venda objVenda = new Venda();
        objVenda.setTotal(valor);
        objVenda.setListaItens(listaItens);
        objVenda.setDataVenda(DataVenda);
        objVenda.setBuscarCliente(cpfVenda);
        objVenda.setNomeCliente(nomeCliente);

        Produto objProduto = new Produto();
        objProduto.setNomeProduto(nomeProduto);

        boolean retorno = VendaDao.salvar(objVenda);
        if (retorno){
            AtualizaEstoque();
            limparTextoGeral();
            JOptionPane.showMessageDialog(this, "Venda feita com sucesso!");
            valorTotal = 0;
            qtdeTotal = 0;

        } else{
            JOptionPane.showMessageDialog(this, "Falha na gravação!");
        }                // TODO add your handling code here:
    }//GEN-LAST:event_btnFinalizarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        int linha = tbVenda.getSelectedRow();

        int qtdTb = Integer.parseInt(tbVenda.getValueAt(linha, 2).toString());
        double valorTb = Double.parseDouble(tbVenda.getValueAt(linha, 3).toString());

        int qtd = Integer.parseInt(txtCarrinho.getText());
        double valor = Integer.parseInt(txtTotal1.getText());

        int valorLinha = (int) (qtdTb * valorTb);
        valorTotal = (int) (valor - valorLinha);

        qtdeTotal = qtd - qtdTb;

        txtTotal1.setText(String.valueOf(valorTotal));
        txtCarrinho.setText(Integer.toString(qtdeTotal));

        DefaultTableModel modelo = (DefaultTableModel) tbVenda.getModel();
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void AtualizaEstoque() {
        int estoqueSaida;
        String codProduto;
        
        for (int i = 0; i < tbVenda.getRowCount(); i++) {
            codProduto = Validador.objectToString(tbVenda.getValueAt(i, 0));
            estoqueSaida = Validador.objectToInt(tbVenda.getValueAt(i, 2));
              
            Produto produto = new Produto();
            ProdutoDAO produtoDao = new ProdutoDAO();
            
            produto.setCodProduto(codProduto);
            produto.setQuantidadeProduto(estoqueSaida);
            produtoDao.atualizarEstoque(produto);
        }
    }
    
    private void limparTextoGeral() {
            
        jdcDataVenda.setDate(null);
        txtBuscarCliente.setText("");
        txtVendedor.setSelectedItem("Selecione...");
        txtEstoque.setText("");        
        txtProduto.setText("");
        txtQtde.setValue(0);
        txtValor.setText("");
        txtNomeCliente.setText("");
        txtDescricaoProduto.setText("");
        txtCarrinho.setText("");
        txtTotal1.setText(""); 
        
        DefaultTableModel modelo = (DefaultTableModel) tbVenda.getModel();
        modelo.setRowCount(0);
    }
    
       private void limparTexto() {
        
        txtProduto.setText("");
        txtDescricaoProduto.setText("");
        txtQtde.setValue(0);
        txtValor.setText("");
        txtEstoque.setText("");
               
    }
        
    private void txtBuscarClienteFocusLost(java.awt.event.FocusEvent evt) {                                           
        // TODO add your handling code here:

        if(!txtBuscarCliente.getText().trim().equals("")){
            String id = txtBuscarCliente.getText();
            Clientes prod = ClienteDAO.consultar(id);

            if (prod != null) {
                txtNomeCliente.setText(prod.getNome()); 
            } else {
                JOptionPane.showMessageDialog(null, "Cliente não encontrado!");
                limparTextoGeral();
            }
        }
    }               
    private void fmtTxtBuscaCpfActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_fmtTxtBuscaCpfActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_fmtTxtBuscaCpfActionPerformed

    private void btnBuscaCpfActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnBuscaCpfActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_btnBuscaCpfActionPerformed

    private void fmtTxtBuscaCodProdActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_fmtTxtBuscaCodProdActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_fmtTxtBuscaCodProdActionPerformed

    private void btnBuscaCodProdActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnBuscaCodProdActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_btnBuscaCodProdActionPerformed

    private void fmtTxtNumVendaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_fmtTxtNumVendaActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_fmtTxtNumVendaActionPerformed

    private void btnAdicionarAoCarrinhoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnAdicionarAoCarrinhoActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_btnAdicionarAoCarrinhoActionPerformed

    private void txtQntdItensCarrinhoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txtQntdItensCarrinhoActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_txtQntdItensCarrinhoActionPerformed

    private void txtValorTotalPedidoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txtValorTotalPedidoActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_txtValorTotalPedidoActionPerformed

    private void btnRealizarVendaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnRealizarVendaActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_btnRealizarVendaActionPerformed

    private void btnCancelarVendaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCancelarVendaActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_btnCancelarVendaActionPerformed

    private void btnInicioActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnInicioActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_btnInicioActionPerformed

    private void cbxQntdProdVendaMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_cbxQntdProdVendaMouseClicked
        // TODO add your handling code here:
    }// GEN-LAST:event_cbxQntdProdVendaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
        // (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the default
         * look and feel.
         * For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaVenda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnFinalizar;
    private javax.swing.JButton btnInterir;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JCalendar jCalendar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private com.toedter.calendar.JMonthChooser jMonthChooser1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private com.toedter.calendar.JDateChooser jdcDataVenda;
    private javax.swing.JTable tbVenda;
    private javax.swing.JFormattedTextField txtBuscarCliente;
    private javax.swing.JTextField txtCarrinho;
    private javax.swing.JTextField txtDescricaoProduto;
    private javax.swing.JTextField txtEstoque;
    private javax.swing.JTextField txtNomeCliente;
    private javax.swing.JTextField txtProduto;
    private javax.swing.JSpinner txtQtde;
    private javax.swing.JTextField txtTotal1;
    private javax.swing.JTextField txtValor;
    private javax.swing.JComboBox<String> txtVendedor;
    // End of variables declaration//GEN-END:variables
}
