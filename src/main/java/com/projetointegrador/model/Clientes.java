/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.projetointegrador.model;

import java.util.Date;

/**
 *
 * @author Rafael
 */
public class Clientes {
   private int codCliente;
   private String dataNasc;
   private String nome;
   private String cpf;
   private String email;
   private String sexo;
   private String telefone;
   private  String endereco;
   private  String nEndereco;
   private String cidade;
   private String cep;
   private String tipoEndereco;

    public String getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(String dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getnEndereco() {
        return nEndereco;
    }

    public void setnEndereco(String nEndereco) {
        this.nEndereco = nEndereco;
    }

    public String getCidade() {
        return nEndereco;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoEndereco() {
        return tipoEndereco;
    }

    public void setTipoEndereco(String tipoEndereco) {
        this.tipoEndereco = tipoEndereco;
    }
    
    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int CodCliente) {
        this.codCliente = codCliente;
    }
}
