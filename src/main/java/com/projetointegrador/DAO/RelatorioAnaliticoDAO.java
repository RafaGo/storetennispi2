/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.projetointegrador.DAO;

import com.projetointegrador.model.ItemVenda;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rafael
 */
public class RelatorioAnaliticoDAO {
    
    public static String url = "jdbc:mysql://localhost:3306/projeto";
    public static String login = "root";
    public static String senha = ""; 
    
   public static ArrayList<ItemVenda> listar(String nome, String dataCompra) {
        
        Connection conexao = null;
        ArrayList<ItemVenda> lista = new ArrayList<>();
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            conexao = DriverManager.getConnection(url,login,senha);            
            
            PreparedStatement comandoSQL = conexao.prepareStatement("select nome, data_compra, cod_produto, nome_produto, qtd, valor from itemvenda where nome = ? and data_compra = ?"); 
            comandoSQL.setString(1, nome);
            comandoSQL.setString(2, dataCompra);
            ResultSet rs = comandoSQL.executeQuery();
            
            if(rs!=null){
                while(rs.next()){
                    ItemVenda novoObj = new ItemVenda();
                    novoObj.setCod_produto(rs.getInt("cod_produto"));
                    novoObj.setValor(rs.getDouble("valor"));
                    novoObj.setQtd(rs.getInt("qtd"));
                    novoObj.setNome_produto(rs.getString("nome_produto"));
                    
                    
                    lista.add(novoObj);
                }
            }
        
            } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return lista;
     }
}