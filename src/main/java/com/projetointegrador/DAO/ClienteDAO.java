package com.projetointegrador.DAO;


import com.projetointegrador.model.Clientes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rafael
 */
public class ClienteDAO {        
    public static String url = "jdbc:mysql://localhost:3306/projeto";
    public static String login = "root";
    public static String senha = "";
    
    public static boolean salvar(Clientes obj){
        
        Connection conexao = null;
        boolean retorno = false;
        
        try {
           
            Class.forName("com.mysql.cj.jdbc.Driver");

            conexao = DriverManager.getConnection(url,login,senha);
            
            PreparedStatement comandoSQL = conexao.prepareStatement("INSERT INTO Cliente ( CPF, Nome, Nascimento , Endereco, Sexo, Email,Telefone , Cep , Numero, Estado, TipoEndereco) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
           
            //comandoSQL.setInt(1, obj.getCodCliente());
            comandoSQL.setString(1, obj.getCpf());
            comandoSQL.setString(2,obj.getNome());
            comandoSQL.setString(3,obj.getDataNasc());
            comandoSQL.setString(4,obj.getEndereco());
            comandoSQL.setString(5,obj.getSexo());
            comandoSQL.setString(6,obj.getEmail());
            comandoSQL.setString(7,obj.getTelefone());
            comandoSQL.setString(8,obj.getCep());
            comandoSQL.setString(9,obj.getnEndereco());
            comandoSQL.setString(10,obj.getCidade());
            comandoSQL.setString(11,obj.getTipoEndereco());
           

            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return retorno;
    }
    
    
    public static Clientes consultar(String cpf) {
        Clientes clienteRetorno = null;
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conexao = DriverManager.getConnection(url,login,senha);
            
            PreparedStatement comandoSQL = conexao.prepareStatement("SELECT * FROM Cliente WHERE CPF = ?");
            comandoSQL.setString(1, cpf);
            
            ResultSet rs = comandoSQL.executeQuery();

            if (rs != null) {
                if (rs .next()) {
                    clienteRetorno = new Clientes();
                    clienteRetorno.setNome(rs.getString("Nome"));
                    
                }
            }
            
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return clienteRetorno;
        
    }
    
    public static ArrayList<Clientes> listar() {
        
        Connection conexao = null;
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            conexao = DriverManager.getConnection(url,login,senha);            
            
            PreparedStatement comandoSQL = conexao.prepareStatement("SELECT * FROM Cliente");           
            ResultSet rs = comandoSQL.executeQuery();
            
            if(rs!=null){
                while(rs.next()){
                    Clientes novoObj = new Clientes();
                    novoObj.setCpf(rs.getString("CPF"));
                    novoObj.setNome(rs.getString("Nome"));
                    novoObj.setCpf(rs.getString("Cpf"));
                    novoObj.setDataNasc(rs.getString("Nascimento"));
                    novoObj.setEmail(rs.getString("Email"));
                    novoObj.setTelefone(rs.getString("Telefone"));
                    novoObj.setEndereco(rs.getString("Endereco"));
                    novoObj.setSexo(rs.getString("Sexo"));
                    novoObj.setnEndereco(rs.getString("Numero"));
                    novoObj.setCep(rs.getString("Cep"));
                    novoObj.setCidade(rs.getString("Estado"));
                   
                    lista.add(novoObj);
                }
            }
        
            } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return lista;
     }
    
    
    public static ArrayList<Clientes> listarPorArgumento(String tipo, String arg) {
        String argumento = tipo + " " + "like '" + arg + "%'";
        
        Connection conexao = null;
        
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            conexao = DriverManager.getConnection(url,login,senha);            
            
            PreparedStatement comandoSQL = conexao.prepareStatement("SELECT CPF , Nome, Nascimento , Sexo, Email,Telefone , Cep , Numero, Estado, TipoEndereco FROM Cliente where " + argumento + "");           
            ResultSet rs = comandoSQL.executeQuery();
            
            if(rs!=null){
                while(rs.next()){
                    Clientes novoObj = new Clientes();
                    novoObj.setCpf(rs.getString("CPF"));
                    novoObj.setNome(rs.getString("Nome"));
                    novoObj.setDataNasc(rs.getString("Nascimento"));
                    novoObj.setSexo(rs.getString("Sexo"));
                    novoObj.setTelefone(rs.getString("Telefone"));
                    novoObj.setCep(rs.getString("Cep"));
                    novoObj.setnEndereco(rs.getString("Numero"));
                    novoObj.setCidade(rs.getString("Estado"));
                    novoObj.setTipoEndereco(rs.getString("TipoEndereco"));
                
                    lista.add(novoObj);
                }
            }
        
            } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return lista;
     }
    
     public static boolean atualizar(Clientes obj){
        
        Connection conexao = null;
        boolean retorno = false;
        
        try {
            
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            conexao = DriverManager.getConnection(url,login,senha);
            
            PreparedStatement comandoSQL = conexao.prepareStatement("UPDATE Cliente SET Nome = ?, Nascimento = ?, Endereco = ?, Sexo = ?, Email = ?, Telefone = ?, Cep = ?, Numero = ?, Estado = ?, TipoEndereco = ? WHERE CPF = ?");
            
            comandoSQL.setString(1,obj.getNome());
            comandoSQL.setString(2,obj.getDataNasc());
            comandoSQL.setString(3,obj.getEndereco());
            comandoSQL.setString(4,obj.getSexo());
            comandoSQL.setString(5,obj.getEmail());
            comandoSQL.setString(6,obj.getTelefone());
            comandoSQL.setString(7,obj.getCep());
            comandoSQL.setString(8,obj.getnEndereco());
            comandoSQL.setString(9,obj.getCidade());
            comandoSQL.setString(10,obj.getTipoEndereco());
            comandoSQL.setString(11,obj.getCpf());

            

            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return retorno;
    }
     
     public static boolean excluir(String CPF){
        Connection conexao = null;
        boolean retorno = false;
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            conexao = DriverManager.getConnection(url,login,senha);
            
            PreparedStatement comandoSQL = conexao.prepareStatement("DELETE FROM Cliente WHERE Cpf = ?");
            comandoSQL.setString(1,CPF);            
            
            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return retorno;
    }
}
