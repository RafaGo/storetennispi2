/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.projetointegrador.DAO;

import static com.projetointegrador.DAO.ClienteDAO.login;
import static com.projetointegrador.DAO.ClienteDAO.senha;
import static com.projetointegrador.DAO.ClienteDAO.url;
import com.projetointegrador.model.Clientes;
import com.projetointegrador.model.Produto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rafael
 */
public class ProdutoDAO {
    public static String url = "jdbc:mysql://localhost:3306/projeto";
    public static String login = "root";
    public static String senha = "";
    
    public static boolean salvar(Produto obj){
        
        Connection conexao = null;
        boolean retorno = false;
        
        try {
            
            //TODO: Implementar insert na tabela NotaFiscal
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            //Abrir a conexão
            conexao = DriverManager.getConnection(url,login,senha);
                
            //Criar o comando sql
            PreparedStatement comandoSQL = conexao.prepareStatement("INSERT INTO Produto (CodigoProd ,TipoProd, MarcaProd ,CorProd ,NomeProd,DescProd,ValorProd,QntdPrd) VALUES(?,?,?,?,?,?,?,?)");
            comandoSQL.setString(1,obj.getCodProduto());
            comandoSQL.setString(2,obj.getTipoProduto());
            comandoSQL.setString(3,obj.getMarcaProduto());
            comandoSQL.setString(4,obj.getCorProduto());
            comandoSQL.setString(5,obj.getNomeProduto());
            comandoSQL.setString(6,obj.getDescricaoProduto());
            comandoSQL.setDouble(7,obj.getValorProduto());
            comandoSQL.setInt(8,obj.getQuantidadeProduto());
            
            //Executar o comando
            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            }
            
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return retorno;
    }
    
    
    public static Produto consultarPorID(int ID) {
        Produto produtoRetorno = null;
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conexao = DriverManager.getConnection(url,login,senha);
            
            PreparedStatement comandoSQL = conexao.prepareStatement("SELECT * FROM Produto WHERE CodigoProd = ?");
            comandoSQL.setInt(1, ID);
            
            ResultSet rs = comandoSQL.executeQuery();

            if (rs != null) {
                if (rs .next()) {
                    produtoRetorno = new Produto();
                    produtoRetorno.setNomeProduto(rs.getString("NomeProd"));
                    produtoRetorno.setValorProduto(rs.getDouble("ValorProd"));
                    produtoRetorno.setQuantidadeProduto(rs.getInt("QntdPrd"));
                }
            }
            
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return produtoRetorno;
        
    }
    
    public static ArrayList<Produto> listar(){
        
        Connection conexao = null;
        ArrayList<Produto> lista = new ArrayList<Produto>();
        
        try {
            //Implementar consulta à tabela NotaFiscal
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            //Abrir a conexão
            conexao = DriverManager.getConnection(url,login,senha);
            
            //Criar o comando SQL
            PreparedStatement comandoSQL =
            conexao.prepareStatement("SELECT * FROM Produto");
            
            //Executar o comando
            ResultSet rs = comandoSQL.executeQuery();
            
            if(rs!=null){
                while(rs.next()){
                    Produto novoObjeto = new Produto();
                    novoObjeto.setCodProduto(rs.getString("CodigoProd"));
                    novoObjeto.setNomeProduto(rs.getString("NomeProd"));
                    novoObjeto.setValorProduto(rs.getDouble("ValorProd"));
                    novoObjeto.setQuantidadeProduto(rs.getInt("QntdPrd"));
                    novoObjeto.setMarcaProduto(rs.getString("MarcaProd"));
                    novoObjeto.setDescricaoProduto(rs.getString("DescProd"));
                    novoObjeto.setTipoProduto(rs.getString("TipoProd"));
                    novoObjeto.setCorProduto(rs.getString("CorProd"));


                    lista.add(novoObjeto);
                    
                }
            }                  
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return lista;
    }
    
   public static ArrayList<Produto> listarPorArgumento(String tipo, String arg) {
        String argumento = tipo + " " + "like '" + arg + "%'";
        
        Connection conexao = null;
        
        ArrayList<Produto> lista = new ArrayList<Produto>();
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            conexao = DriverManager.getConnection(url,login,senha);            
            
            PreparedStatement comandoSQL = conexao.prepareStatement("SELECT CodigoProd , TipoProd, MarcaProd , CorProd, NomeProd,DescProd , ValorProd , QntdPrd  FROM Produto Where " + argumento + "");           
            ResultSet rs = comandoSQL.executeQuery();
            
            
            if(rs!=null){
                while(rs.next()){
                  Produto novoObjeto = new Produto();
                    novoObjeto.setCodProduto(rs.getString("CodigoProd"));
                    novoObjeto.setNomeProduto(rs.getString("NomeProd"));
                    novoObjeto.setValorProduto(rs.getDouble("ValorProd"));
                    novoObjeto.setQuantidadeProduto(rs.getInt("QntdPrd"));
                    novoObjeto.setMarcaProduto(rs.getString("MarcaProd"));
                    novoObjeto.setDescricaoProduto(rs.getString("DescProd"));
                    novoObjeto.setTipoProduto(rs.getString("TipoProd"));
                    novoObjeto.setCorProduto(rs.getString("CorProd"));
                    
                    lista.add(novoObjeto);
                }
            }
        
            } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return lista;
     }
    public static ResultSet listarPorNome (String tipo, String arg) throws SQLException {
        String argumento = tipo + " " + "like '" + arg + "%'";
        
        Connection conexao = null;
        
        PreparedStatement comandoSQL = conexao.prepareStatement("SELECT NomeProd, CPF FROM cliente where " + argumento + "");
        //comandoSQL.setString(1, "%" + NomeProd + "%");
        ResultSet rs = comandoSQL.executeQuery();
        
        return rs;        
    }
    
    
    public static boolean atualizar(Produto obj){
        
        Connection conexao = null;
        boolean retorno = false;
        
        try {
            
            //TODO: Implementar insert na tabela NotaFiscal
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            //Abrir a conexão
            conexao = DriverManager.getConnection(url,login,senha);
            
            //Criar o comando sql
          
           
            PreparedStatement comandoSQL = conexao.prepareStatement("UPDATE  Produto SET TipoProd =? ,  MarcaProd = ? ,CorProd =? ,NomeProd =?,DescProd =?,ValorProd =?,QntdPrd=? WHERE CodigoProd =?");
            comandoSQL.setString(1,obj.getTipoProduto());
            comandoSQL.setString(2,obj.getMarcaProduto());
            comandoSQL.setString(3,obj.getCorProduto());
            comandoSQL.setString(4,obj.getNomeProduto());
            comandoSQL.setString(5,obj.getDescricaoProduto());
            comandoSQL.setDouble(6,obj.getValorProduto());
            comandoSQL.setInt(7,obj.getQuantidadeProduto());
            comandoSQL.setString(8,obj.getCodProduto());

            
                       
            //Executar o comando
            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return retorno;
    }
    
public static boolean atualizarEstoque(Produto obj){
        
        Connection conexao = null;
        boolean retorno = false;
        
        try {
            
            //TODO: Implementar insert na tabela NotaFiscal
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            //Abrir a conexão
            conexao = DriverManager.getConnection(url,login,senha);
            
            //Criar o comando sql
            PreparedStatement comandoSQL = conexao.prepareStatement("UPDATE Produto SET QntdPrd = QntdPrd - ? WHERE CodigoProd = ?");
            comandoSQL.setInt(1,obj.getQuantidadeProduto());
            comandoSQL.setString(2,obj.getCodProduto());
                       
            //Executar o comando
            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return retorno;
    }
    
    public static boolean excluir(String CodProduto){
    
        Connection conexao = null;
        boolean retorno = false;
        
        try {
            
            //TODO: Implementar insert na tabela NotaFiscal
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            //Abrir a conexão
            conexao = DriverManager.getConnection(url,login,senha);
            
            //Criar o comando sql
            PreparedStatement comandoSQL = conexao.prepareStatement("DELETE FROM Produto WHERE CodigoProd =?");
            comandoSQL.setString(1,CodProduto);            
            
            //Executar o comando
            int linhasAfetadas = comandoSQL.executeUpdate();
            if(linhasAfetadas>0){
               retorno = true;
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return retorno;
    
    }
}

